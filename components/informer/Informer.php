<?php


namespace yii2portal\weather\components\informer;

use Yii;

use yii2portal\core\components\Widget;
use yii2portal\weather\models\Weather;

class Informer extends Widget
{

    public function insert($view)
    {
        $weather = [];
        $items = Weather::find()
            ->where(['enabled' => 1])
            ->orderBy(['ord' => SORT_ASC])
            ->all();

        $timeShift = Yii::$app->getModule('weather')->timeSdv;
        foreach ($items as $item) {
            $weather[$item->id] = (object) unserialize($item->weather);
        }

        return $this->render($view, [
            'items' => $items,
            'timeShift' => $timeShift,
            'weather' => $weather
        ]);

    }
}