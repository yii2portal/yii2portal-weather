<?php

namespace yii2portal\weather\models;

/**
 * This is the ActiveQuery class for [[Weather]].
 *
 * @see Weather
 */
class WeatherQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Weather[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Weather|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
