<?php

namespace yii2portal\weather\models;

use Yii;

/**
 * This is the model class for table "weather".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $weather
 * @property integer $ord
 * @property integer $enabled
 * @property integer $dateline
 */
class Weather extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'weather';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['weather'], 'string'],
            [['ord', 'enabled', 'dateline'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'weather' => 'Weather',
            'ord' => 'Ord',
            'enabled' => 'Enabled',
            'dateline' => 'Dateline',
        ];
    }

    /**
     * @inheritdoc
     * @return WeatherQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WeatherQuery(get_called_class());
    }
}
